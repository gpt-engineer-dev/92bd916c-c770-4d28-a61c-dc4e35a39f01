document.getElementById('add-note').addEventListener('click', function() {
    const title = document.getElementById('note-title').value;
    const content = document.getElementById('note-content').value;

    if (title && content) {
        const notes = JSON.parse(localStorage.getItem('notes')) || [];
        notes.push({ title, content });
        localStorage.setItem('notes', JSON.stringify(notes));

        document.getElementById('note-title').value = '';
        document.getElementById('note-content').value = '';

        displayNotes();
    }
});

function displayNotes() {
    const notes = JSON.parse(localStorage.getItem('notes')) || [];
    const notesElement = document.getElementById('notes');

    notesElement.innerHTML = '';
    for (let i = 0; i < notes.length; i++) {
        const note = notes[i];
        const noteElement = document.createElement('div');
        noteElement.classList.add('p-4', 'border', 'border-gray-300', 'rounded', 'bg-white');
        noteElement.innerHTML = `
            <h2 class="text-xl mb-2">${note.title}</h2>
            <p>${note.content}</p>
            <button class="mt-4 p-2 bg-red-500 text-white rounded" onclick="deleteNote(${i})">Delete</button>
        `;
        notesElement.appendChild(noteElement);
    }
}

function deleteNote(index) {
    const notes = JSON.parse(localStorage.getItem('notes')) || [];
    notes.splice(index, 1);
    localStorage.setItem('notes', JSON.stringify(notes));
    displayNotes();
}

displayNotes();
